//
// Created by Adrian Karlsen on 20/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUENTRY_H
#define C_SIMPLEMENU_SIMPLEMENUENTRY_H

#include "../SimpleMenuConstants.h"


typedef struct SimpleMenuEntryPrototype SimpleMenuEntry;

SimpleMenuEntry* SIMPLE_MENU_ENTRY_init();

void SIMPLE_MENU_ENTRY_setId(SimpleMenuEntry* menuEntry, String* id);

void SIMPLE_MENU_ENTRY_setText(SimpleMenuEntry* menuEntry, String* text);

void SIMPLE_MENU_ENTRY_setExecutionFunction(SimpleMenuEntry* menuEntry, void (*exFunc));

String* SIMPLE_MENU_ENTRY_getId(const SimpleMenuEntry* menuEntry);

String* SIMPLE_MENU_ENTRY_getText(const SimpleMenuEntry* menuEntry);

void (*SIMPLE_MENU_ENTRY_getExecutionFunction(const SimpleMenuEntry* menuEntry))();

void* SIMPLE_MENU_ENTRY_destroy(const SimpleMenuEntry* menuEntry);

size_t SIMPLE_MENU_ENTRY_objectSize();

#endif //C_SIMPLEMENU_SIMPLEMENUENTRY_H

//
// Created by Adrian Karlsen on 21/01/2021.
//

#include "../../Presenter/SimpleMenuViewModelAdapter.h"


SimpleMenuViewModel* SIMPLE_MENU_VIEW_MODEL_ADAPTER_mapToViewModel(SimpleMenuEntry* menuEntry) {
    SimpleMenuViewModel* viewModel = SIMPLE_MENU_VM_init();

    SIMPLE_MENU_VM_setId(viewModel, SIMPLE_MENU_ENTRY_getId(menuEntry));
    SIMPLE_MENU_VM_setText(viewModel, SIMPLE_MENU_ENTRY_getText(menuEntry));

    return viewModel;
}

SimpleMenuEntry* SIMPLE_MENU_VIEW_MODEL_ADAPTER_mapToMenuEntry(SimpleMenuViewModel* viewModel) {

}



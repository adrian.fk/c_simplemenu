//
// Created by Adrian Karlsen on 20/01/2021.
//

#include "../../SimpleMenuEntry.h"

struct SimpleMenuEntryPrototype {
    String* id;
    String* text;
    void (*executionFunc);
};

void defaultExecutionHandler() {}


SimpleMenuEntry* SIMPLE_MENU_ENTRY_init() {
    struct SimpleMenuEntryPrototype* entry = (SimpleMenuEntry*) malloc(sizeof(struct SimpleMenuEntryPrototype));

    entry->id = STRING_init();
    entry->text = STRING_init();
    entry->executionFunc = &defaultExecutionHandler;

    return entry;
}

void SIMPLE_MENU_ENTRY_setId(SimpleMenuEntry* menuEntry, String* id) {
    STRING_setString(menuEntry->id, id);
}

void SIMPLE_MENU_ENTRY_setText(SimpleMenuEntry* menuEntry, String* text) {
    STRING_setString(menuEntry->text, text);
}

void SIMPLE_MENU_ENTRY_setExecutionFunction(SimpleMenuEntry* menuEntry, void (*exFunc)) {
    menuEntry->executionFunc = exFunc;
}

String* SIMPLE_MENU_ENTRY_getId(const SimpleMenuEntry* menuEntry) {
    return menuEntry->id;
}

String* SIMPLE_MENU_ENTRY_getText(const SimpleMenuEntry* menuEntry) {
    return menuEntry->text;
}

void (*SIMPLE_MENU_ENTRY_getExecutionFunction(const SimpleMenuEntry* menuEntry))() {
    return menuEntry->executionFunc;
}

void* SIMPLE_MENU_ENTRY_destroy(const SimpleMenuEntry* menuEntry) {
    STRING_destroy(menuEntry->id);
    STRING_destroy(menuEntry->text);
    free(menuEntry->executionFunc);
}

size_t SIMPLE_MENU_ENTRY_objectSize() {
    return sizeof(struct SimpleMenuEntryPrototype);
}

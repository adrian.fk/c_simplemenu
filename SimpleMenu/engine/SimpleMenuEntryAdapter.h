//
// Created by Adrian Karlsen on 21/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUENTRYADAPTER_H
#define C_SIMPLEMENU_SIMPLEMENUENTRYADAPTER_H

#include "SimpleMenuEntry.h"

void SIMPLE_MENU_ENTRY_ADAPTER_mapFromPrimitivesToMenuEntry(SimpleMenuEntry* _dest,
                                                            String* id,
                                                            String* text,
                                                            void (*executionFunctionCall));


SimpleMenuEntry* SIMPLE_MENU_ENTRY_ADAPTER_mapFromPrimitivesToNewMenuEntry(String* id,
                                                                           String* text,
                                                                           void (*executionFunctionCall));


#endif //C_SIMPLEMENU_SIMPLEMENUENTRYADAPTER_H

//
// Created by Adrian Karlsen on 20/01/2021.
//

#include "../SimpleMenuConsoleView.h"

#define separationLine '-'

#define outString(string) printf("%s", STRING_getCharArr(string))
#define outCharArr(charArr) printf("%s", charArr)
#define outNewLine printf("\n")
#define outSeparationLine printf("%c", separationLine)

#define setOutColor(colorCode) printf("\033%s", colorCode)
#define resetOutColor printf("\033[0m")

#define COLOR_CODE_RED "[1;31m"

#define DEFAULT_MENU_WIDTH 50
#define DEFAULT_MENU_LEFT_PADDING 5



struct SimpleMenuConsoleViewPrototype {
    int longestString;
    int paddingHeader;
    int numCharsWidth;
    String* headerTitle;
};

SimpleMenuConsoleView* SIMPLE_MENU_CONSOLE_VIEW_init() {
    struct SimpleMenuConsoleViewPrototype* v = (struct SimpleMenuConsoleViewPrototype*) malloc(sizeof(struct SimpleMenuConsoleViewPrototype));

    v->headerTitle = STRING_init();
    v->numCharsWidth = DEFAULT_MENU_WIDTH;

    return v;
}

void displaySeparationLine(const SimpleMenuConsoleView *view) {
    for (int i = 0; i < view->numCharsWidth; ++i) {
        outSeparationLine;
    }
}

void handleHeaderRendering(const SimpleMenuConsoleView *view, String *headerTitle, int i) {
    if (i == 0 || (i == view->numCharsWidth - 1)) {
        outCharArr("|");
    }
    else if (i < view->paddingHeader || (i >= view->paddingHeader + STRING_length(headerTitle) && i != view->numCharsWidth - 1)) {
        outCharArr(" ");
    }
    else if (i == view->paddingHeader) {
        outString(headerTitle);
    }
}

void displayAHeader(SimpleMenuConsoleView *view, String *headerTitle) {
    view->paddingHeader = (view->numCharsWidth - STRING_length(headerTitle)) / 2;
    for (int i = 0; i < view->numCharsWidth; ++i) {
        handleHeaderRendering(view, headerTitle, i);
    }
}

void SIMPLE_MENU_CONSOLE_VIEW_displayThisHeader(SimpleMenuConsoleView* view, String* headerTitle) {
    displaySeparationLine(view);
    if (STRING_getLength(headerTitle)) {
        outNewLine;
        displayAHeader(view, headerTitle);
        outNewLine;
        displaySeparationLine(view);
    }
}

void SIMPLE_MENU_CONSOLE_VIEW_displayHeader(SimpleMenuConsoleView* view) {
    SIMPLE_MENU_CONSOLE_VIEW_displayThisHeader(view, view->headerTitle);
}

void SIMPLE_MENU_CONSOLE_VIEW_setHeaderTitle(SimpleMenuConsoleView* view, String* headerTitle) {
    STRING_setString(view->headerTitle, headerTitle);
}

void SIMPLE_MENU_CONSOLE_VIEW_clearConsoleView() {
    for (int i = 0; i < 51; ++i) {
        outNewLine;
    }
}

void SIMPLE_MENU_CONSOLE_VIEW_setLongestMenuItemString(SimpleMenuConsoleView* view, int longestMenuItemString){
    view->longestString = longestMenuItemString;
    //int minNumChars = 2;
}

void SIMPLE_MENU_CONSOLE_VIEW_displayFooter(SimpleMenuConsoleView* view) {
    outNewLine;
    displaySeparationLine(view);
}

void SIMPLE_MENU_CONSOLE_VIEW_displayMenuEntry(SimpleMenuConsoleView* view, SimpleMenuViewModel* viewModel) {
    outNewLine;
    int separatorStringLength = 3;
    int entryIdStartIndx = DEFAULT_MENU_LEFT_PADDING + STRING_length(SIMPLE_MENU_VM_getId(viewModel));
    int entryTextStartIndx = entryIdStartIndx + separatorStringLength;
    int entryTextEndIndx = entryTextStartIndx + STRING_length(SIMPLE_MENU_VM_getText(viewModel)) - 1;

    for (int i = 0; i < view->numCharsWidth; ++i) {
        if (i == 0 || i == view->numCharsWidth - 1) outCharArr("|");
        else if ((i > 0 && i < DEFAULT_MENU_LEFT_PADDING) || i > entryTextEndIndx) outCharArr(" ");
        else if (i == entryIdStartIndx) outString(SIMPLE_MENU_VM_getId(viewModel));
        else if (i == entryIdStartIndx + STRING_length(SIMPLE_MENU_VM_getId(viewModel))) outCharArr(" - ");
        else if (i == entryTextStartIndx) outString(SIMPLE_MENU_VM_getText(viewModel));

    }

    /*
    outCharArr("\t");
    outString(SIMPLE_MENU_VM_getId(viewModel));
    outCharArr(" - ");
    outString(SIMPLE_MENU_VM_getText(viewModel));
    */
}

void SIMPLE_MENU_CONSOLE_VIEW_displayErrorMsg(String* errMsg) {
    outNewLine;
    setOutColor(COLOR_CODE_RED);
    outString(errMsg);
    resetOutColor;
}

String* SIMPLE_MENU_CONSOLE_VIEW_getInput() {
    outNewLine;
    outCharArr("\t:: ");
    String* input = STRING_init();
    STRING_getInput(input);
    return input;
}

void SIMPLE_MENU_CONSOLE_VIEW_destroy(SimpleMenuConsoleView* view) {
    STRING_destroy(view->headerTitle);
}


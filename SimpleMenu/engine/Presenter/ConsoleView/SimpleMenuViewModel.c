//
// Created by Adrian Karlsen on 21/01/2021.
//

#include "../SimpleMenuViewModel.h"

struct SimpleMenuViewModelPrototype {
    String* id;
    String* text;
};


SimpleMenuViewModel* SIMPLE_MENU_VM_init() {
    struct SimpleMenuViewModelPrototype* vm = (struct SimpleMenuViewModelPrototype*) malloc(sizeof(struct SimpleMenuViewModelPrototype));

    vm->id = STRING_init();
    vm->text = STRING_init();

    return vm;
}

void SIMPLE_MENU_VM_setId(SimpleMenuViewModel* viewModel, String* keybind) {
    STRING_setString(viewModel->id, keybind);
}

void SIMPLE_MENU_VM_setText(SimpleMenuViewModel* viewModel, String* text) {
    STRING_setString(viewModel->text, text);
}

String* SIMPLE_MENU_VM_getId(SimpleMenuViewModel* viewModel) {
    return viewModel->id;
}

String* SIMPLE_MENU_VM_getText(SimpleMenuViewModel* viewModel) {
    return viewModel->text;
}

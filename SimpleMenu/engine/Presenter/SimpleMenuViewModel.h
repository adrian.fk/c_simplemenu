//
// Created by Adrian Karlsen on 21/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUVIEWMODEL_H
#define C_SIMPLEMENU_SIMPLEMENUVIEWMODEL_H

#include "../../SimpleMenuConstants.h"

typedef struct SimpleMenuViewModelPrototype SimpleMenuViewModel;

SimpleMenuViewModel* SIMPLE_MENU_VM_init();

void SIMPLE_MENU_VM_setId(SimpleMenuViewModel* viewModel, String* keybind);

void SIMPLE_MENU_VM_setText(SimpleMenuViewModel* viewModel, String* text);

String* SIMPLE_MENU_VM_getId(SimpleMenuViewModel* viewModel);

String* SIMPLE_MENU_VM_getText(SimpleMenuViewModel* viewModel);


#endif //C_SIMPLEMENU_SIMPLEMENUVIEWMODEL_H

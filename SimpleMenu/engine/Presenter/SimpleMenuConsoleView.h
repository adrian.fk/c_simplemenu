//
// Created by Adrian Karlsen on 20/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUCONSOLEVIEW_H
#define C_SIMPLEMENU_SIMPLEMENUCONSOLEVIEW_H

#include "../../SimpleMenuConstants.h"
#include "SimpleMenuViewModel.h"


typedef struct SimpleMenuConsoleViewPrototype SimpleMenuConsoleView;

SimpleMenuConsoleView* SIMPLE_MENU_CONSOLE_VIEW_init();

void SIMPLE_MENU_CONSOLE_VIEW_displayHeader(SimpleMenuConsoleView* view);

void SIMPLE_MENU_CONSOLE_VIEW_displayThisHeader(SimpleMenuConsoleView* view, String* headerTitle);

void SIMPLE_MENU_CONSOLE_VIEW_setHeaderTitle(SimpleMenuConsoleView* view, String* headerTitle);

void SIMPLE_MENU_CONSOLE_VIEW_clearConsoleView();

void SIMPLE_MENU_CONSOLE_VIEW_setLongestMenuItemString(SimpleMenuConsoleView* view, int longestMenuItemString);

void SIMPLE_MENU_CONSOLE_VIEW_displayFooter(SimpleMenuConsoleView* view);

void SIMPLE_MENU_CONSOLE_VIEW_displayMenuEntry(SimpleMenuConsoleView* view, SimpleMenuViewModel* viewModel);

void SIMPLE_MENU_CONSOLE_VIEW_displayErrorMsg(String* errMsg);

String* SIMPLE_MENU_CONSOLE_VIEW_getInput();

void SIMPLE_MENU_CONSOLE_VIEW_destroy(SimpleMenuConsoleView* view);

#endif //C_SIMPLEMENU_SIMPLEMENUCONSOLEVIEW_H

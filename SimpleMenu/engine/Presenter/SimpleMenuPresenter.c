//
// Created by Adrian Karlsen on 20/01/2021.
//

#include "../SimpleMenuPresenter.h"

struct SimpleMenuPresenterPrototype {
    int mode;
    SimpleMenuConsoleView* consoleView;
};

void handleDefaultMode(struct SimpleMenuPresenterPrototype *presenter);

SimpleMenuPresenter* SIMPLE_MENU_PRESENTER_init() {
    struct SimpleMenuPresenterPrototype* p = (struct SimpleMenuPresenterPrototype*) malloc(sizeof(struct SimpleMenuPresenterPrototype));

    p->mode = MODE_DEFAULT;
    p->consoleView = SIMPLE_MENU_CONSOLE_VIEW_init();

    handleDefaultMode(p);

    return p;
}

void handleDefaultMode(struct SimpleMenuPresenterPrototype *presenter) {
    if (presenter->mode == MODE_DEFAULT) {
        presenter->mode = MODE_CONSOLE;
    }
}

void SIMPLE_MENU_PRESENTER_setMode(SimpleMenuPresenter* presenter, Mode mode) {
    if (mode <= MODE_MAX_VALUE) presenter->mode = mode;
}

void SIMPLE_MENU_PRESENTER_setMenuTitle(SimpleMenuPresenter* presenter, String* title) {
    SIMPLE_MENU_CONSOLE_VIEW_setHeaderTitle(presenter->consoleView, title);
}

void SIMPLE_MENU_PRESENTER_presentMenu(SimpleMenuPresenter* presenter, SimpleMenuContainer* menuContainer, const Bool* error) {
    SIMPLE_MENU_CONSOLE_VIEW_clearConsoleView();
    SIMPLE_MENU_CONSOLE_VIEW_setHeaderTitle(presenter->consoleView, SIMPLE_MENU_CONTAINER_getMenuTitle(menuContainer));
    SIMPLE_MENU_CONSOLE_VIEW_displayHeader(presenter->consoleView);

    for (int i = 0; i < SIMPLE_MENU_CONTAINER_containerLength(menuContainer); ++i) {
        SimpleMenuViewModel* target = SIMPLE_MENU_VIEW_MODEL_ADAPTER_mapToViewModel(
                SIMPLE_MENU_CONTAINER_get(menuContainer, i)
        );
        SIMPLE_MENU_CONSOLE_VIEW_displayMenuEntry(presenter->consoleView, target);
    }

    SIMPLE_MENU_CONSOLE_VIEW_displayFooter(presenter->consoleView);
    if (*error) SIMPLE_MENU_CONSOLE_VIEW_displayErrorMsg(SIMPLE_MENU_CONTAINER_getErrorMsg(menuContainer));
}

void SIMPLE_MENU_PRESENTER_destroy(SimpleMenuPresenter* presenter) {
    SIMPLE_MENU_CONSOLE_VIEW_destroy(presenter->consoleView);
    free(presenter->consoleView);
}

String* SIMPLE_MENU_PRESENTER_getInput() {
    return SIMPLE_MENU_CONSOLE_VIEW_getInput();
}

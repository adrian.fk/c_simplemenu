//
// Created by Adrian Karlsen on 20/01/2021.
//

#include "../SimpleMenuController.h"

struct SimpleMenuControllerPrototype {
    SimpleMenuContainer* menuContainer;
    SimpleMenuPresenter* presenter;
};

void processInput(const SimpleMenuController* controller, String *input, Bool* exit, Bool* error);

SimpleMenuController* SIMPLE_MENU_CONTROLLER_init() {
    SimpleMenuController* o = (SimpleMenuController*) malloc(sizeof(struct SimpleMenuControllerPrototype));

    o->menuContainer = SIMPLE_MENU_CONTAINER_init();
    o->presenter = SIMPLE_MENU_PRESENTER_init();

    return o;
}

void run(const SimpleMenuController *controller) {
    Boolean exit = FALSE;
    Boolean error = FALSE;
    String* input;

    do {
        SIMPLE_MENU_PRESENTER_presentMenu(controller->presenter, controller->menuContainer, &error);
        input = SIMPLE_MENU_PRESENTER_getInput();
        processInput(controller, input, &exit, &error);
    } while (!exit);

    STRING_destroy(input);
}

void SIMPLE_MENU_CONTROLLER_commit(const SimpleMenuController* controller) {
    run(controller);
}

void processInput(const SimpleMenuController* controller, String* input, Bool* exit, Bool* error) {
    if (STRING_equals(input, SIMPLE_MENU_CONTAINER_getExitStatement(controller->menuContainer))) {
        *exit = TRUE;
    }
    else {
        int containerLength = SIMPLE_MENU_CONTAINER_containerLength(controller->menuContainer);
        *error = TRUE;
        for (int i = 0; i < containerLength; ++i) {
            SimpleMenuEntry* target = SIMPLE_MENU_CONTAINER_get(controller->menuContainer, i);
            String* targetId = SIMPLE_MENU_ENTRY_getId(target);
            if (STRING_equals(targetId, input)) {
                if (NULL != SIMPLE_MENU_ENTRY_getExecutionFunction(target))
                    SIMPLE_MENU_ENTRY_getExecutionFunction(target)();
                *error = FALSE;
                break;
            }
        }
    }
}

void SIMPLE_MENU_CONTROLLER_addMenuItem(SimpleMenuController* controller,
                                        String* id,
                                        String* text,
                                        void (*executionFunctionCall)) {

    if (!SIMPLE_MENU_CONTAINER_containsId(controller->menuContainer, id))
    SIMPLE_MENU_CONTAINER_addMenuEntry(
        controller->menuContainer,
        SIMPLE_MENU_ENTRY_ADAPTER_mapFromPrimitivesToNewMenuEntry(
                id, text, executionFunctionCall
        )
    );
}

void SIMPLE_MENU_CONTROLLER_removeItemById(SimpleMenuController* controller, String* id) {
    SIMPLE_MENU_CONTAINER_removeEntryById(controller->menuContainer, id);
}

void SIMPLE_MENU_CONTROLLER_removeItemByIndex(SimpleMenuController* controller, const int index) {
    SIMPLE_MENU_CONTAINER_removeEntryByIndex(controller->menuContainer, index);
}

void SIMPLE_MENU_CONTROLLER_setMenuTitle(SimpleMenuController* controller, String* menuTitle) {
    SIMPLE_MENU_CONTAINER_setMenuTitle(controller->menuContainer, menuTitle);
}

void SIMPLE_MENU_CONTROLLER_setExitStatement(SimpleMenuController* controllerObj, String* exitStatement) {
    SIMPLE_MENU_CONTAINER_setExitStatement(controllerObj->menuContainer, exitStatement);
}

void SIMPLE_MENU_CONTROLLER_setErrorMsg(SimpleMenuController* controller, String* errMsg) {
    SIMPLE_MENU_CONTAINER_setErrorMsg(controller->menuContainer, errMsg);
}

void SIMPLE_MENU_CONTROLLER_destroy(SimpleMenuController* controllerObj) {
    SIMPLE_MENU_CONTAINER_destroy(controllerObj->menuContainer);
    free(controllerObj->menuContainer);
    SIMPLE_MENU_PRESENTER_destroy(controllerObj->presenter);
    free(controllerObj->presenter);
}

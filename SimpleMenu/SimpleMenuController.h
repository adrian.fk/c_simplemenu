//
// Created by Adrian Karlsen on 20/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUCONTROLLER_H
#define C_SIMPLEMENU_SIMPLEMENUCONTROLLER_H

#include "engine/SimpleMenuContainer.h"
#include "engine/SimpleMenuEntryAdapter.h"
#include "engine/SimpleMenuPresenter.h"


typedef struct SimpleMenuControllerPrototype SimpleMenuController;

SimpleMenuController* SIMPLE_MENU_CONTROLLER_init();

void SIMPLE_MENU_CONTROLLER_commit(const SimpleMenuController* simpleMenuControllerObj);

void SIMPLE_MENU_CONTROLLER_addMenuItem(SimpleMenuController* controller,
                                        String* id,
                                        String* text,
                                        void (*executionFunctionCall));


void SIMPLE_MENU_CONTROLLER_removeItemById(SimpleMenuController* simpleMenuControllerObj, String* id);

void SIMPLE_MENU_CONTROLLER_removeItemByIndex(SimpleMenuController* simpleMenuControllerObj, int index);

void SIMPLE_MENU_CONTROLLER_setMenuTitle(SimpleMenuController* controllerObj, String* menuTitle);

void SIMPLE_MENU_CONTROLLER_setExitStatement(SimpleMenuController* controllerObj, String* exitStatement);

void SIMPLE_MENU_CONTROLLER_setErrorMsg(SimpleMenuController* controller, String* errMsg);

void SIMPLE_MENU_CONTROLLER_destroy(SimpleMenuController* controllerObj);


#endif //C_SIMPLEMENU_SIMPLEMENUCONTROLLER_H

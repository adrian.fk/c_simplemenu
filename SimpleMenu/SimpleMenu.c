//
// Created by Adrian Karlsen on 20/01/2021.
//

#include "../SimpleMenu.h"

SimpleMenuController* controller;

Bool isAllocated() {
    return NULL != controller;
}

void SIMPLE_MENU_commit() {
    if (isAllocated()) SIMPLE_MENU_CONTROLLER_commit(controller);
}

void SIMPLE_MENU_init() {
    controller = SIMPLE_MENU_CONTROLLER_init();
}

void SIMPLE_MENU_createMenuItem(char* id,
                                char* text,
                                void (*executionFunctionCall)) {

    if (isAllocated()) {
        String* stringId = STRING_init();
        String* stringText = STRING_init();

        STRING_set(stringId, id);
        STRING_set(stringText, text);

        SIMPLE_MENU_CONTROLLER_addMenuItem(
                controller,
                stringId,
                stringText,
                executionFunctionCall
        );
    }
}

void SIMPLE_MENU_removeItemById(char* id) {
    if (isAllocated()) {
        String* idString = STRING_init();
        STRING_set(idString, id);
        SIMPLE_MENU_CONTROLLER_removeItemById(controller, idString);
    }
}

void SIMPLE_MENU_removeItemByIndex(int index) {
    if (isAllocated()) SIMPLE_MENU_CONTROLLER_removeItemByIndex(controller, index);
}

void SIMPLE_MENU_setMenuTitle(char* menuTitle) {
    String* menuTitleString = STRING_init();

    STRING_set(menuTitleString, menuTitle);

    SIMPLE_MENU_CONTROLLER_setMenuTitle(controller, menuTitleString);

    STRING_destroy(menuTitleString);
}

void SIMPLE_MENU_setExitStatement(char* exitStatement) {
    String* exitStatementString = STRING_init();

    STRING_set(exitStatementString, exitStatement);

    SIMPLE_MENU_CONTROLLER_setExitStatement(controller, exitStatementString);

    STRING_destroy(exitStatementString);
}

void SIMPLE_MENU_setErrorMsg(char* errorMessage) {
    String* errorMsg = STRING_init();

    STRING_set(errorMsg, errorMessage);

    SIMPLE_MENU_CONTROLLER_setErrorMsg(controller, errorMsg);

    STRING_destroy(errorMsg);
}

void SIMPLE_MENU_destroy() {
    if (isAllocated()) {
        SIMPLE_MENU_CONTROLLER_destroy(controller);
        free(controller);
    }
}
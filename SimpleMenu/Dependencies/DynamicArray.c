#include <stdio.h>
#include "DynamicArray.h"

#define convertIndex(lgth) lgth - 1

#define DEFAULT_ALLOCATION 10
#define MAX_ALLOCATION 100

#define ERROR_HANDLED 0

#define FIRST_ELEMENT 1
#define GENERAL_ELEMENT 2

typedef struct Element{
    float sortable;
    void* data;
}Element;

struct ArrayPrototype {
    int error;
    int length;
    size_t dataSize;
    Element* arr;
    int allocated;
    int allocationSize;
};

void processFirstElement(Array arrObj, void *input);
void processGeneralElement(Array arrObj, void *input);


struct ArrayPrototype* ARRAY_constructor(size_t _elementSize) {
    struct ArrayPrototype* arrToInit = malloc(sizeof(struct ArrayPrototype));
    arrToInit->length = 0;
    arrToInit->dataSize = _elementSize;
    arrToInit->allocated = DEFAULT_ALLOCATION;
    arrToInit->allocationSize = DEFAULT_ALLOCATION;
    return arrToInit;
}

int ARRAY_getLength(Array arrObj) {
    return arrObj->length;
}

void ARRAY_setErrorHandled(Array arr) {
    arr->error = ERROR_HANDLED;
}

void ARRAY_setAllocationSize(Array arr, int allocationSize) {
    if(allocationSize > DEFAULT_ALLOCATION && allocationSize < MAX_ALLOCATION)
    arr->allocationSize = allocationSize;
}

void swap(void* a,void* b, size_t size)
{
    void *temp = malloc(size);

    if (temp) {
        memcpy(temp, a, size);
        memcpy(a, b, size);
        memcpy(b, temp, size);
        free(temp);
    }
}

void ARRAY_swap(Array arrObj, int xIndex, int yIndex) {
    float tmpSortable = arrObj->arr[xIndex].sortable;
    arrObj->arr[xIndex].sortable = arrObj->arr[yIndex].sortable;
    arrObj->arr[yIndex].sortable = tmpSortable;

    swap(
            ARRAY_get(arrObj, xIndex),
            ARRAY_get(arrObj, yIndex),
            arrObj->dataSize
    );

}

void* ARRAY_get(struct ArrayPrototype* arrObj, int index) {
    if(index < arrObj->length) {
        return arrObj->arr[index].data;
    }
    arrObj->error = ARRAY_ERROR_GET_OOB;
    return NULL;
}

float ARRAY_getSortable(struct ArrayPrototype *arrObj, int index) {
    if(index < arrObj->length) {
        return arrObj->arr[index].sortable;
    }
    arrObj->error = ARRAY_ERROR_GET_SORTABLE_OOB;
    return 0;
}

int ARRAY_getError(struct ArrayPrototype* arr) {
    return arr->error;
}

void process(int processType, Array arrObj, void* input) {
    void (*processFunctionPtrArr[])(Array, void*) = {processFirstElement, processGeneralElement};
    (*processFunctionPtrArr[convertIndex(processType)])(arrObj, input);
}

void processGeneralElement(Array arrObj, void *input) {
    if(arrObj->length == arrObj->allocated) {
        arrObj->allocated += arrObj->allocationSize;
        size_t new_size = arrObj->allocated * sizeof(Element);
        arrObj->arr = realloc(arrObj->arr, new_size);

    }

    if(!arrObj->arr) {
        arrObj->error = ARRAY_ERROR_MALLOC;
    }
    else {
        arrObj->arr[convertIndex(arrObj->length)].data = malloc(arrObj->dataSize);
        if(!arrObj->arr[convertIndex(arrObj->length)].data) {
            arrObj->error = ARRAY_ERROR_MALLOC_MEMBER;
        }
        else {
            memcpy(arrObj->arr[convertIndex(arrObj->length)].data, input, arrObj->dataSize);
        }
    }
}

void processFirstElement(Array arrObj, void *input) {
    arrObj->arr = malloc(arrObj->allocationSize * sizeof(struct Element));
    if(!arrObj->arr) {
        arrObj->error = ARRAY_ERROR_MALLOC;
    }
    else{
        arrObj->arr[convertIndex(arrObj->length)].data = malloc(arrObj->dataSize);
        if(!arrObj->arr[convertIndex(arrObj->length)].data){
            arrObj->error = ARRAY_ERROR_MALLOC_MEMBER;
        }
        else {
            memcpy(arrObj->arr[convertIndex(arrObj->length)].data, input, arrObj->dataSize);
        }
    }
}

void ARRAY_setSortable(struct ArrayPrototype *arrObj, int index, float sortable) {
    if(!arrObj->arr[convertIndex(arrObj->length)].data) {
        arrObj->error = ARRAY_ERROR_NO_SORATABLE_DATA;
    }
    else {
        arrObj->arr[index].sortable = sortable;
    }
}

void ARRAY_add(struct ArrayPrototype* arrObj, void* input) {
    arrObj->length += 1;
    if(FIRST_ELEMENT == arrObj->length) {
        process(FIRST_ELEMENT, arrObj, input);
    }
    else{
        process(GENERAL_ELEMENT, arrObj, input);
    }
}

void ARRAY_set(Array arrObj, int index, void* input){
    if(index < arrObj->length) {
        memcpy(arrObj->arr[index].data, input, arrObj->dataSize);
    }
    else{
        arrObj->error = ARRAY_ERROR_SET_OOB;
    }

}

void ARRAY_remove(Array array, int index) {
    if (index < ARRAY_getLength(array)) {
        size_t elementSize = array->dataSize;
        //Create new array
        Array tmp = ARRAY_constructor(elementSize);

        //Add each member BUT the member at index
        for (int i = 0; i < ARRAY_getLength(array); ++i) {
            if (i != index) {
                ARRAY_add(
                        tmp,
                        ARRAY_get(array, i)
                );
            }
        }


        //Replace existing array with new array
        ARRAY_destroy(array);
        array = ARRAY_constructor(elementSize);
        memcpy(array, tmp, elementSize);
    }
}


void ARRAY_destroy(Array arrObj){
    //TODO Clear element as well?

    if(arrObj->arr) {
        /*
        for (int i = convertIndex(arrObj->length); i >= 0; i--) {
            if (arrObj->arr[i].data) {
                void* tmp = arrObj->arr[i].data;
                int tmy = (int) tmp;
                free(tmp);
                arrObj->arr[i].data = NULL;
            }
        }*/
        free(arrObj->arr);
        arrObj->arr = NULL;
        free(arrObj);
        arrObj = NULL;
    }
}
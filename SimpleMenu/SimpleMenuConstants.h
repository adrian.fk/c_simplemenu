//
// Created by Adrian Karlsen on 20/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUCONSTANTS_H
#define C_SIMPLEMENU_SIMPLEMENUCONSTANTS_H


// MODULE DEPENDENCIES
#include <stdlib.h>
#include <stdio.h>

#include "Dependencies/SimplefiedStrings.h"
#include "Dependencies/DynamicArray.h"


// GENERAL CONSTANT PRE-PROCESS MACROS
#define TRUE 1
#define FALSE 0
#define CONVERT_NUMBER_TO_INDEX(num) (num - 1)
#define CONVERT_INDEX_TO_NUMBER(index) (index + 1)


// SIMPLE MENU CONSTANT PRE-PROCESS MACROS


// GENERAL TYPE DEFINITIONS
typedef int Boolean;
typedef int Bool;

// SIMPLE MENU TYPE DEFINITIONS




#endif //C_SIMPLEMENU_SIMPLEMENUCONSTANTS_H

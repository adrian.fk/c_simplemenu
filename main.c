#include <stdio.h>
#include "SimpleMenu.h"



void printHello() {
    printf("Hello!");
}


/**
 * @Objective: This method is to simply display how to use the SimpleMenu module.
 * @return
 */
int main() {
    SIMPLE_MENU_init();

    SIMPLE_MENU_setMenuTitle("Test Header");

    SIMPLE_MENU_createMenuItem(
            "1",
            "Test",
            &printHello
    );

    SIMPLE_MENU_createMenuItem(
            "2",
            "Test2",
            NULL
    );

    SIMPLE_MENU_createMenuItem(
            "3",
            "Test3",
            &printHello
    );

    SIMPLE_MENU_createMenuItem(
            "4",
            "Test4",
            &printHello
    );

    SIMPLE_MENU_createMenuItem(
            "5",
            "Test5",
            &printHello
    );


    SIMPLE_MENU_removeItemById("1");

    //SIMPLE_MENU_setExitStatement("e");
    //SIMPLE_MENU_setErrorMsg("Wrong input");

    SIMPLE_MENU_commit();

    SIMPLE_MENU_destroy();

    return 0;
}

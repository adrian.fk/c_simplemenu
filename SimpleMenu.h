//
// Created by Adrian Karlsen on 20/01/2021.
//
/**
 *  @Objective: Facade for SimpleMenu module
 */

#ifndef C_SIMPLEMENU_SIMPLEMENU_H
#define C_SIMPLEMENU_SIMPLEMENU_H

#include "SimpleMenu/SimpleMenuConstants.h"
#include "SimpleMenu/SimpleMenuController.h"


void SIMPLE_MENU_init();

void SIMPLE_MENU_commit();

void SIMPLE_MENU_createMenuItem(char* id,
                                char* text,
                                void (*executionFunctionCall));

void SIMPLE_MENU_removeItemById(char* id);

void SIMPLE_MENU_removeItemByIndex(int index);

void SIMPLE_MENU_setMenuTitle(char* menuTitle);

void SIMPLE_MENU_setExitStatement(char* exitStatement);

void SIMPLE_MENU_setErrorMsg(char* errorMessage);

void SIMPLE_MENU_destroy();

#endif //C_SIMPLEMENU_SIMPLEMENU_H
